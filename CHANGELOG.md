# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.1.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/compare/v2.1.1...v2.1.2) (2021-07-21)


### Bug Fixes

* define IMAGES_TO_RELEASE in bash file ([04ce142](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/commit/04ce1428db4aa1c8c5d11efad3135e37b97b84a1))

### [2.1.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/compare/v2.1.0...v2.1.1) (2021-07-21)


### Bug Fixes

* release-image releases specific image ([af7c112](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/commit/af7c112b25d15b21d3b7b8ca611ad359978eecaa))

## [2.1.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/compare/v2.0.0...v2.1.0) (2021-07-21)


### Features

* new file ([403c4c8](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/commit/403c4c81c2718cab33fa0424f8e789a61269d008))

## [2.0.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/compare/v1.0.1...v2.0.0) (2021-07-21)


### ⚠ BREAKING CHANGES

* added automatic versioning

### Features

* added automatic versioning ([1994f8d](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend/commit/1994f8dc8a124a87a81ed258da0ed8042c0fc248))

### 1.0.1 (2021-07-21)
