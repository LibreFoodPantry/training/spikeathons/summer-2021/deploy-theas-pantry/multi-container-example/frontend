#!/bin/bash

# All the comments below have been written by sohoda. The script commands have been written by Stoney.
# Refer to this link for the original bash script file: https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2020/release-image/-/blob/master/entrypoint.bash

set -xue # x: print commands and arguments as they are executed
         # u: treat unset variables and parameters other than the special
         #    parameters ‘@’ or ‘*’ as an error when performing parameter expansion
         # e: exit pipeline if non-zero value is returned
         # refer to this link for more details: https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html


# IMPORTANT: Every time the script is run w/o argument, the patch number
# is incremented accordingly. Perhaps similarly to manually running standard-version.
# TODO: Determine how to increment other numbers besides the patch number.

main(){
  standard-version "$@" # standard-version should take an argument from the command
			# line regarding the current version of the image in order to
                        # determine how the version number will change.

  VERSION="$(git describe --abbrev=0)" # finds the most recent tag readable from
                                       # a commit. could take the most recent version
                                       # tag of the image based on the commit.
                                       # the value is identical to that of the git tag.

  VERSION_1_2_3="${VERSION#v}"        # these lines seem to simply change the format
  VERSION_1_2="${VERSION_1_2_3%.*}"   # of the versioning tag and assign that value
  VERSION_1="${VERSION_1_2%.*}"       # to the given variables

  # push tags and files command for github/gitlab repo
  PUBLISH_COMMANDS="git push --follow-tags origin main" # this string is used as
                                                            # output to publish the release.
                                                            # supposedly this is to be run independently
                                                            # by the person working on the current release
                                                            # rather than be run automatically by the script

  # this parses the names (single, whitespace-separated) of images to be released
  # and assigns the string value to variable IMAGES
  # refer to this link for more details (first upvoted answer, second example): https://stackoverflow.com/questions/27296904/read-ra-vs-direct-assignment
  IMAGES_TO_RELEASE=registry.gitlab.com/librefoodpantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/frontend:latest

  IFS=' ' read -ra IMAGES <<< "$IMAGES_TO_RELEASE"
  for IMAGE in "${IMAGES[@]}"; do
    BASE="${IMAGE%:*}"
    docker tag "${IMAGE}" "${BASE}:${VERSION_1_2_3}" # tags the release image based on specified versioning format
    docker tag "${IMAGE}" "${BASE}:${VERSION_1_2}"   # ditto
    docker tag "${IMAGE}" "${BASE}:${VERSION_1}"     # ditto
    docker tag "${IMAGE}" "${BASE}:latest"           # tags the release image as 'latest'
    # the command below changes the string variable's value that will be
    # output once the script finishes executing.
    # refer to comment on line 27
    # push command for docker repo, tag the image (BASE) using the specified formats
    # though it doesn't seem to push the 'latest' tag
    PUBLISH_COMMANDS="${PUBLISH_COMMANDS}docker push ${BASE}:${VERSION_1_2_3}\ndocker push ${BASE}:${VERSION_1_2}\ndocker push ${BASE}:${VERSION_1}\n"
    # example of output: git push --follow-tags origin main
    #                    docker push ubuntu:1.0.1
    #                    docker push ubuntu:1.0
    #                    docker push ubuntu:1
  done

  printf '\n\nWhen you are ready to publish the release, run the following commands:\n\n%b\n\n' "${PUBLISH_COMMANDS}"

}

main "$@"

